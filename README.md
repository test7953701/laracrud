# Laravel CRUD

## Errores

>[php artisan migration => error](https://stackoverflow.com/questions/42244541/laravel-migration-error-syntax-error-or-access-violation-1071-specified-key-wa)
>
> - Faltaba agregar el engine en config/database.php => "engine": de null a "InnoDB".
> - Eliminar las tablas creadas con errores en la base de datos.

## Comandos utiles

`composer create-project laravel/laravel <NombreProyecto>` => crear proyecto en laravel

`php artisan --version` => conocer la version del framework

`php artisan config:cache` => limpiar cache

`php artisan migrate` => migrar los php en la carpeta migration a la base de datos especificada en `.env`

>>> [tipos de datos para las migraciones](https://laravel.com/docs/10.x/migrations#available-column-types)

`php artisan make:model <NombreModelo> -mcrfp` => crea todo los recursos necesario para una nueva tabla en la base de datos (migration, controller, resource, factory, pivot)

`php artisan route:list` => para mostrar todas las rutas activas en el proyecto

## Metodos utiles

### Metodos de modelos

- `Clase::paginate(10)` => en este caso se toman los 10 primeros registros de la base de datos creada en base al modelo por medio del comando `make:model`
- `Clase::all()`
- `Clase::select()`
- `Clase::where()`
- `Clase::json()`
- `Clase::redirect()`
- `Clase::destroy(numeroID)` => Podria utilizar `$id` como parametro de la funcion destroy del controlador (no entiendo porque). Entonces en lugar de usar la variable de la clase y apuntar a la propiedad `$crud['id']` usaria directamente `$id` en el metodo destroy de la clase:
    ```php
      //pasaria de esto
      public function destroy(crud $crud)
      {
      crud::destroy($crud['id']);
      return redirect('/crud');
      }
      //a esto
      public function destroy($id)
      {
      crud::destroy($id);
      return redirect('/crud');
      }
    ```

## Notas

- <code>Route::get('users/{id}', [UserController::class, 'index']) `->name('user.index')`;</code> el name se usa para cuando lo quiero apuntar a esa ruta con laravel

- Cuidar como inicio de las peticiones a los modelos, lo conveniente seria filtrar los datos antes de pedirlos a la base de datos. Si lo hiciera al reves no estaria usando las querys de la base de datos y le estaria pidiendo a laravel que traiga todos los registros y los filtre.

- Limitar accesos al servicio usando request personalizadas, que no sea el controlador el que haga las validaciones, esto para evitar cuellos de botella.

- Protect Hidden en los modelos para no mostrar algunos datos del mismo

- Pone en mayuscula la primera letra del `make:model` (creo, porque se genera una clase y creo que por convension va la primera en mayu, ¿? consultar despues a Gary)

- Crear registros de prueba con `/database/factories && seeders`:
  >- Primero definir una factory para el modelo `EjemploFactory.php`
  > ```php
  >class CrudFactory extends Factory
  >{
  >    public function definition(): array
  >    {
  >        return [
  >            'Name' => fake()->name(),
  >            'Type' => fake()->name(),
  >            'Color' => fake()->name(),
  >            'Price' => fake()-> name(),
  >        ];
  >    }
  >}
  > ```
  >
  > - Y luego definir la semilla en DatabaseSeeder.php metodo run()
  >```php
  >   use Illuminate\Database\Seeder;
  >   class DatabaseSeeder extends Seeder
  >   {
  >       public function run()
  >       {
  >           \App\Models\crud::factory(10)->create();
  >           // Crea 10 registros con datos generados por la fábrica
  >       }
  >   }
  >```
  > #### Por ultimo para iniciar: ```php artisan db:seed```
  >
  > #### Para regenerar y volver a cargar todas las semillas: ```php artisan migrate:refresh --seed```
  > [`Faker Formatters`](https://github.com/fzaninotto/Faker?tab=readme-ov-file#formatters)



### Pasar de `Get` a `Resource` ❌
- El metodo `get` permite adjuntar una ruta (primer parametro) a una vista (segundo parametro). 
- En el segundo parametro puedo utilizar clases y sus metodos correspondientes
- Entonces en un metodo de la clase pasada en el segundo parametro coloco la vista que quiero

- Para optimizar mas las cosas puedo usar el metodo `resource`
el cual permite utilizar todos los metodos de una clase con sus respectivos nombres a los cuales les adjuntamos vistas

  > Resource esta bien para empezar pero no conviene, es mejor crear los propios metodos con los request y vincularlos a los endpoints que correspondan ✔

### Enviar archivos o fotos por formulario

Para esto en el formulario `post` le agregamos el atributo `enctype=multipart/form-data`

### Para evitar ataques CSRF

Laravel requiere que en cada formulario se utilice el helper `@csrf`.
Esto crea un token unico por usuario logueado, por lo que ayuda a evitar peticiones falisificadas utilizando el logueo de estos usuarios.

Ejemplo de token `"_token": "eLjqtNEPzEuvlVu2ZJFGoKXqgiJ52fDclPAIHaz7"` ✌

[Documentacion sobre CSRF](https://laravel.com/docs/10.x/csrf)

### Otras directivas

#### `@include` para reciclar codigo en vistas
#### `@is` Operador para las vistas, osea que tenemos todos los operadores como directivas
#### `@json` directiva para ver los datos que llegan al cliente por medio de un json. Ejemplo de script para la vista: 👇
  ```js
  <script type="text/javascript">
      console.log(@json($registro));
  </script>
  ```
#### `isset()` Es como un operador ternario para las vistas


## Investigar

- Propiedades y Metodos estaticos y no estaticos en las clases.
- Relacionar modelos, forma estandar y forma optima de hacerlo.
- Encadenamientos de funciones.
- Como hago para enviar el token por medio de peticiones postman
- Cambiar el nombre de un modelo sobre la marcha (pendiente para futuro muy futuro)
- Problema de 1 + n (se soluciona con with - buscar).
- Abstract function.
- Tablas intermedias.
- Crear validaciones listadas por G.

