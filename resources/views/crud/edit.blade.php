
<form action="{{ url('/api/'.$registro->id) }}" method="post">
    @csrf
    @method('PUT')
    @include('crud.form')
</form>



<script type="text/javascript">
    console.log(@json($registro));
</script>