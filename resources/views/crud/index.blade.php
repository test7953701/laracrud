Mostrar lista

@if (Session::has('mensaje'))
{{Session::get('mensaje')}}
@endif
<a href="{{ url('/crud/create')}}">Crear registro</a>
<table class="table table-striped table-inverse table-responsive">
    <thead class="thead-inverse">
        <tr>
            <th>#</th>
            <th>Nombre</th>
            <th>Tipo</th>
            <th>Color</th>
            <th>Precio</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach( $registros as $registro)
        <tr>
            <td>{{ $registro -> id}}</td>
            <td>{{ $registro -> Name}}</td>
            <td>{{ $registro -> Type}}</td>
            <td>{{ $registro -> Color}}</td>
            <td>{{ $registro -> Price}}</td>
            <td> {{-- 
                http://localhost/crudLaravel/public/crud/5/edit --}}
                {{-- <form action="{{ url('/crud/'.$registro -> id.'/edit')}}" method="post">
                    @csrf
                    {{ method_field('PUT')}}
                    <input type="submit" value="Editar">
                </form>  --}}
                <a href="{{ url('/crud/'.$registro -> id.'/edit')}}">Editar</a>
                | 
                <form action="{{ url('/api/'.$registro -> id)}}" method="post">
                    @csrf
                    {{-- {{ method_field('DELETE')}}  --}}
                    @method('DELETE')
                    <input type="submit" onclick="return confirm('Segurito?')" value="Borrar">
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{-- <script type="text/javascript">
    console.log(@json($registros).data);
</script> --}}