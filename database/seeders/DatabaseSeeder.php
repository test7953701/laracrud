<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        \App\Models\crud::factory(10) -> create();
        // \App\Models\Post::factory(40) -> create();
        \App\Models\Attribute::factory(20) -> create();
        \App\Models\Category::factory(5) -> create();
        \App\Models\CrudCategoryRelation::factory(20) -> create();
    }
}
