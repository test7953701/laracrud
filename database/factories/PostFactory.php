<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;



class PostFactory extends Factory
{
    public function definition(): array
    {
        return [
            'crud_id' => $this->faker->numberBetween($min = 1,$max = 20),
            'comments' => $this->faker->text($maxNbChars = 200),
        ];
    }
}
