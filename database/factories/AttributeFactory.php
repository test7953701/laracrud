<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Attribute>
 */
class AttributeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $zonaOptions = ['Norte' , 'Sur' ,'Este' ,'Oeste'];

    public function definition(): array
    {
        return [
            'age' => $this->faker->numberBetween($min = 10, $max = 90),
            'zona' => $this->faker->randomElement($this->zonaOptions),
            'email' => $this->faker->freeEmail,
        ];

    }

}
