<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\CrudCategoryRelation>
 */
class CrudCategoryRelationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'crud_id' => $this->faker->numberBetween($min = 1,$max = 10),
            'category_id' => $this->faker->numberBetween($min = 1,$max = 5),
        ];
    }
}
