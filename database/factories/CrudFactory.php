<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use App\Models\crud;

/* $factory->define(crud::class, function (Faker $faker) {
    return [
        'Name' => $faker->name,
        'Type' => $faker->name,
        'Color' => $faker->name,
        'Price' => $faker->name,
    ];
}); */


//preguntar a G como vincular un factory a un modelo
//sin seguir la convencion de los nombres. ❔
// protected $model = crud::class;
//protected $model = \App\Models\crud::class;

class CrudFactory extends Factory
{
    protected $typeOptions = ['Orco', 'Perro', 'Gato'];
    protected $colorOptions = ['Azul', 'Verde', 'Negro'];

    public function definition(): array
    {
        return [
            'Name' => $this->faker->firstName,
            'Type' => $this->faker->randomElement($this->typeOptions),
            'Color' => $this->faker->randomElement($this->colorOptions),
            'Price' => $this->faker->numberBetween($min = 100, $max = 9000),
        ];
    }
}

/* return [
    'Name' => fake()->name(),
    'Type' => fake()->name(),
    'Color' => fake()->name(),
    'Price' => fake()-> name(),
]; */
