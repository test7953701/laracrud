<?php

namespace App\Providers;

use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Model;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Se llama durante la fase de registro de servicios de la aplicacion
     */
    public function register(): void
    {
        /**
         * contenedor de servicios de Laravel ($this->app) 
         * para registrar un singleton del tipo GuzzleHttp\Client
         * 
         * singleton en este contexto significa que solo se creara 
         * una unica instancia de la clase GuzzleHttp\Client durante el 
         * ciclo de vida de la aplicacion
         */
        $this->app->singleton('GuzzleHttp\Client' , function(){
            /**
             * devuelve una nueva instancia de la clase GuzzleHttp\Client 
             * configurada con una URL base y un tiempo de espera
             */
            return new Client([
                'base_uri' => 'https://jsonplaceholder.typicode.com',
                'timeout'  => 2.0,
            ]);
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //investigar ❔
        //Model::preventLazyLoading(! app()->isProduction());
    }
}
