<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    protected $fillable = ['crud_id', 'comments'];
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function relationCrud()
    {
        return $this -> belongsTo(crud::class , 'crud_id');
    }
}
