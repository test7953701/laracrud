<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Contracts\JWTSubject;

class crud extends Model implements JWTSubject
{
    protected $table = 'cruds';
    //🟣 Buscar porque el guarded pero parece necesario
    // para cargar datos con Clase::create()
    protected $guarded = [];  

    //datos del modelo que quiero ocultar
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function complementPost() {
        return $this -> hasOne(Post::class , 'crud_id');
    }

    public function hasManyComplement() {
        return $this -> hasMany(Post::class , 'crud_id');
    }
    public function crudCategories() {
        // return $this -> belongsToMany(Category::class , 'crud_category');
        return $this -> belongsToMany(Category::class , 'crud_category_relations');
    }

        /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier() {
        return $this -> getKey();
    }

        /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims() {
        return [];
    }


    use HasFactory;
}
