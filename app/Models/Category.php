<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function crudCategories() {
        return $this -> belongsToMany(crud::class , 'crud_category_relations');
    }
    use HasFactory;
}
