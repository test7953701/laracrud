<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use App\Services\ApiPost;


class PostController extends Controller
{
    protected $ApiPost;

    public function __construct(ApiPost $ApiPost)
    {
        $this->ApiPost = $ApiPost;
    }

    public function test(){
        $ApiPosts = $this->ApiPost->all();
        foreach ($ApiPosts as $dato) {
            Post::create([
                'crud_id' => $dato->userId,
                'comments' => $dato->body
            ]);
        }
        return response()->json(['Create' => $ApiPosts], 200);
    }

    public function testID($id){
        $ApiPost = $this->ApiPost->find($id);
        return response()->json($ApiPost, 200);
    }

    public function uploadApi(){
        $ApiPosts = $this->ApiPost->all();
        foreach ($ApiPosts as $ApiPost) {
            Post::create([
                'crud_id' => $ApiPost->userId,
                'comments' => $ApiPost->body
            ]);
        }
        return response()->json(['Upload Posts_table' => $ApiPosts], 200);
    }

    public function list()
    {
        $data = Post::all() ;
        return response() -> json([
            'state' => 200,
            'method' => 'get',
            'data' => $data
            ]
            ,200
        );
    }

    public function registerCrud($id) {

        $complement = Post::findOrFail($id);
        $complement -> relationCrud;
        return response() -> json($complement);
    }

    public function store(Request $request)
    {
        $datos = request() -> except('_token');
        Post::insert($datos);
        return response() -> json($datos);
    }
}
