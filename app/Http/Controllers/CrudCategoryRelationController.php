<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\crud;
use Illuminate\Http\Request;


class CrudCategoryRelationController extends Controller
{
    public function crudRelation() {
        $cruds = crud::has('crudCategories') -> with('crudCategories') -> get();
        $result = [];

        foreach ($cruds as $crud) {
            $result[] = [
                'crud' => $crud,
                // 'categories' => $crud->crudCategories
            ];
        }
        return response()->json(['crud' => $result]);
    }
}
