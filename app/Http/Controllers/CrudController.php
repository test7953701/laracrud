<?php

namespace App\Http\Controllers;

use App\Models\crud;
use App\Services\ApiPost;
use Illuminate\Http\Request;
use App\Http\Requests\storeRequest;
use App\Http\Requests\getRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;


class CrudController extends Controller
{
    protected $ApiPost;

    public function __construct(ApiPost $ApiPost)
    {
        $this->ApiPost = $ApiPost;
    }

    public function test(){
        $ApiPost = $this->ApiPost->all();
        return response()->json($ApiPost, 200);
    }

    public function testID($id){
        $ApiPost = $this->ApiPost->find($id);
        return response()->json($ApiPost, 200);
    }

    public function filterQuery(Request $request)
    {
        $Type = $request -> query('type');
        $Color = $request -> query('color');
        $crud = crud::where('Type' , '=' , $Type)
        ->where('Color' , '=', $Color)
        ->get();
        return response()->json($crud, 200);
    }
    public function userComments()
    {
        //Forma fallida de traer todos los comentarios de cada usuario ❔
        /* $data = crud::has('hasManyComplement')
        -> with(['hasManyComplement' => function($query) {
            $query -> select('comments');
        }])
        -> get(); */
        //Me funciono esto ocultando datos no se si es lo optimo ✔
        $data = crud::has('hasManyComplement')
        -> with('hasManyComplement')
        -> get();
        $data -> each(function($user){
            $user -> hasManyComplement -> makeHidden(['id','crud_id']);
        });

        return response() -> json([
            'state' => 200,
            'method' => 'GET',
            'data' => $data
            ]
            ,200
        );
    }

    public function list(getRequest $request)
    {
        $data['registros']= crud::all() ;
        return response() -> json([
            'state' => 200,
            'method' => 'GET',
            'data' => $data
            ]
            ,200
        );
    }

    public function destroy(crud $crud)
    {
        //🟣 darle una mirada a la inyeccion de dependencias
        /* $data = crud::findOrFail($id);
        crud::destroy($id); */
        $crud -> delete();
        $crud -> complementPost() -> delete();
        return response() -> json([
            "status" => 200 ,
            "method" => "delete",
            "data" => $crud
            ],200
        );
    }

    public function register($id)
    {
        $data= crud::findOrFail($id);
        // return response() -> json($data);
        return response() -> json([
            "status" => 200 ,
            "method" => "GET",
            "registro" => $data
            ],200
        );
    }

    public function update(Request $request, $id)
    {
        $datos = request() -> except(['_token','_method']);
        $beforeRegister = crud::findOrFail($id);

        crud::where('id','=',$id) -> update($datos);

        $afterRegister = crud::findOrFail($id);
        return response() -> json([
            "Before" => $beforeRegister,
            "After" => $afterRegister
        ],200);
    }

    //request personalizada antes de entrar al controlador, primero validacion
    //como hago para enviar el token @crf por medio de peticiones postman ❔
    // $datos = request() -> except('_token');
    // crud::insert($datos);
    public function store(storeRequest $request)
    {
        $crud = crud::create([
            'Name' => $request->Name,
            'Type' => $request->Type,
            'Color' => $request->Color,
            'Price' => $request->Price
        ]);
        $token = JWTAuth::fromUser($crud);
        return response() -> json([
            'datos' => $crud,
            'token' => $token
        ]);
    }

    public function testCredentials(Request $request) {
        $credentials = $request-> only('Name');
        try {
            if($token = JWTAuth::attempt($credentials)) {
                return response() -> json([
                    'error' => 'Invalid credentials'
                ], 400);
            }
        } catch (JWTException $e) {
            return response() -> json([
                'error' => 'Not create token'
            ], 500);
        }
        return response()->json(compact('token'));
    }

    public function complement($id) {

        $crud = crud::findOrFail($id);
        if(!$crud) {
            return response() -> json(['errors' => "no se encontro el registro"]);
        }

        $complement = $crud -> hasManyComplement;

        return response() -> json([
            'Status' => 200,
            'Method' => 'GET',
            'Crud' => $crud,
            'Complement' => $complement
        ],200);
    }

    public function getManyComplement($id) {
        $crud = crud::findOrFail($id);
        $crud -> hasManyComplement;
        return response()->json($crud, 200);
    }

/*     public function obtainType($Type) {
        $data = crud::get() -> where('Type' , '=',  $Type);
        $data = crud::where('Type' , '=',  $Type) -> get();
        return response()->json($data, 200);
    } */

    //🔵 coloque un valor por defecto a la variable porque la hice opcional en el parametro de ruta
    public function obtainType($Type = null) {
        //🔵 dd un console.log para inspeccionar los datos
        // dd(route('crud.index',['Type' => $Type]));
        //el resultado de las dos declaraciones es la misma:
        //$datas = crud::get() -> with('complementPost'); //❌
        $datas = crud::with('complementPost')
        -> where('Type' , '=', $Type)
        -> get(); //✔
        //pero en la primera estoy obteniendo todos los datos y depsues filtrando
        //en lugar de hacer el filtrado en la base de datos que esta optimizada para esto.
        foreach ($datas as $data) {
            $data -> complementPost;
            dd($data);
        }
        dd($datas);
        return response()->json($datas, 200);
    }
}
