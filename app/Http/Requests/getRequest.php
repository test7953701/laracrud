<?php

namespace App\Http\Requests;

use App\Http\JsonFormRequest;

class getRequest extends JsonFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        // Hardcodea el token para pruebas (reemplázalo con tu token real en un entorno de producción)
        $hardcodedToken = 'miTokenFicticio123';

        // Verificar si el token proporcionado en la solicitud coincide con el token hardcodeado
        return $this->header('Authorization') === 'Bearer ' . $hardcodedToken;
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [

        ];
    }
}
