<?php

namespace App\Http\Requests;

use App\Http\JsonFormRequest;

//🟣
class storeRequest extends JsonFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */

    //🔵 Aca estoy colocando las primeras validaciones antes de entrar al controlador
    //  /crudLaravel\app\Http\Controllers\CrudController.php
    // buscar como usar jsdoc
    public function rules(): array
    {
        return [
            'Name' => 'required|unique:cruds,Name',
            'Type' => 'required|string|max:10',
            'Color' => 'required|string|max:10',
            'Price' => 'required|numeric|max:5000',
        ];
    }
}
