<?php

namespace App\Http;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest as FormRequest;


//🟣
abstract class JsonFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    abstract public function authorize();

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    abstract public function rules();

    //🔵 Aca estoy devolviendo las validaciones en formato json
    protected function failedValidation(Validator $validator) {

        $error = collect((new ValidationException($validator)) -> errors());
        throw new HttpResponseException(response() -> json(['error' => $error]));
    }
}
