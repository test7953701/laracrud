<?php 
namespace App\Services;

class ApiPost extends GuzzleHttpRequest
{
    public function all() 
    {
        return $this->get();
    }

    public function find($id)
    {
        return $this->get($id);
    }
}
