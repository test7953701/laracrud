<?php 
namespace App\Services;

use GuzzleHttp\Client;

class GuzzleHttpRequest 
{
    protected $client;

    /**
     * Al inyectar el Client ya vienen definidos en la instancia los parametros
     * que fueron agregados en App/Providers/AppServiceProvider#register
     */
    public function __construct(Client $client) {
        $this->client = $client;
    }

    /**
     * Y esto seria un metodo para usar en los hijos de la clase
     * y no repetir el mismo codigo en las distintas peticiones GET
     */
    protected function get($url = '') {
        $response  =  $this -> client -> request ( 'GET' ,  'posts/'.$url );
        return json_decode($response->getBody()->getContents());
    }
}
