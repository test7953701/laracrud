<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CrudController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\CrudCategoryRelationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
/* Crud */

Route::get('/crud/test' , [CrudController::class,'test']);
Route::get('/crud/test/{id}' , [CrudController::class,'testID']);

Route::get('/crud/filterquery' , [CrudController::class,'filterQuery']);

Route::get('/crud/usercomments' , [CrudController::class,'userComments']);

Route::get('/crud/register/{id}' , [CrudController::class,'register']);

//🔵 "?" opcional en el parametro de ruta
Route::get('/crud' , [CrudController::class,'list']);
Route::get('/crud/{Type?}', [CrudController::class, 'obtainType']) ->name('crud.index'); //🔵 el name es para apuntar a una ruta cuando lo usamos en laravel con rute()

Route::post('/crud/create' , [CrudController::class,'store']);
Route::post('/crud/testCre' , [CrudController::class,'testCredentials']);

Route::delete('/crud/delete/{id}' , [CrudController::class,'destroy']);
Route::put('/crud/edit/{id}' , [CrudController::class,'update']);

/* Crud relation with Post */
Route::get('/crud/complement/{id}' , [CrudController::class,'complement']);
Route::get('/crud/{id}/hm_complement' , [CrudController::class,'getManyComplement']);


/* Post */
Route::get('/post/test' , [PostController::class,'test']);
Route::get('/post/test/{id}' , [PostController::class,'testID']);

Route::get('/post' , [PostController::class,'list']);
Route::get('/post/uploadApi' , [PostController::class,'uploadApi']);
Route::post('/post/create' , [PostController::class , 'store']);

/* Post relation with Crud */
Route::get('/post/{id}/register' , [PostController::class,'registerCrud']);

/* Relation Crud Category] */
Route::get('/crud_relation' , [CrudCategoryRelationController::class,'crudRelation']);

